// Set up globals to be used in rendering environment
var scene, renderer, camera;
var controls, clock;
var projector;
var enemy, cube;
var bullets = [];
var loader1 = new THREE.JSONLoader; 
var animation;


// initialization function
function init() 
{
	document.body.style.backgroundColor = '#000000';
	setupThreeJS();
	setupWorld();
	setupCube2();
	setupMarine();
	render();

}

// set up basic of camera, scene, renderer and clock
function setupThreeJS() 
{	// values for camera
	var FOV = 74, ASPECT = window.innerWidth/window.innerHeight, 
		NEAR = 1, FAR = 10000;

	// create scene
	scene = new THREE.Scene();
	//scene.fog = new THREE.FogExp2(0x005abb, 0.001);
	// create camera
	// PerspectiveCamera( fov, aspect, near, far )
	camera = new THREE.PerspectiveCamera( FOV, ASPECT, NEAR, FAR );
	// set camera position
	// Place camera on x axis
	camera.position.set(0,250,600);
	camera.rotation.x = -5 * Math.PI/180
	//camera.lookAt (new THREE.Vector3 (0.0, 0.0, 0.0));

	scene.add( camera );

	// create renderer
	renderer = new THREE.WebGLRenderer({ antialiasing : true });

	// set renderer size
	renderer.setSize( window.innerWidth, window.innerHeight );
	// append renderer to dom
	document.body.appendChild( renderer.domElement );

	// create clock
	clock = new THREE.Clock();
	// add camera controls from library, apply them to camera
	

	var texture = THREE.ImageUtils.loadTexture('face-red.png');
	var material = material = new THREE.MeshBasicMaterial({ map : texture, 
									  overdraw : true
								} );

	geometry = new THREE.CubeGeometry(100, 100, 100);
					
	window.enemy = new THREE.Mesh(geometry, material);


	window.enemy.applyMatrix( new THREE.Matrix4().makeTranslation( 200, 100, 0) );

	scene.add(window.enemy);
	
	window.enemy.add(camera);

	controls = new THREE.FirstPersonControls( window.enemy );
	controls.lookVertical = true;
	// controls vector translation 
	controls.movementSpeed = 750;
	controls.lookSpeed = 0.05;
	controls.lookVertical = false;
	controls.noFly = false;

	var light = new THREE.DirectionalLight( 0xffffff, 1 );
// configure DirectionalLight object to use shadows
	light.position.set( 1, 3, 2);
	light.castShadow = true;
	light.shadowDarkness = 0.5;
	light.shadowMapWidth = 2048;
	light.shadowMapHeight = 2048;
	light.position.set( 500, 1500, 1000 );
	light.shadowCameraFar = 2500;
	// DirectionalLight only; not necessary for PointLight
	light.shadowCameraLeft = -1000;
	light.shadowCameraRight = 1000;
	light.shadowCameraTop = 1000;
	light.shadowCameraBottom = -1000;
	// diplay frustrum
	//light.shadowCameraVisible = true;	
	scene.add( light );

	

}

// function to set up basic world components
function setupWorld()
{
	var texture, floorMaterial, floorMesh, texture2, floorMaterial2, floorMesh2;
	// PlaneGeometry( width, height, widthSegments, heightSegments )

	texture = THREE.ImageUtils.loadTexture( "dirt_sidewalk.png" );
	texture2 = THREE.ImageUtils.loadTexture( "sky_horiz_tile.png" );

	// assuming you want the texture to repeat in both directions:
	texture.wrapS = THREE.RepeatWrapping; 
	texture.wrapT = THREE.RepeatWrapping;
	texture2.wrapS = THREE.RepeatWrapping; 
	texture2.wrapT = THREE.RepeatWrapping;

	// how many times to repeat in each direction; the default is (1,1),
	//   which is probably why your example wasn't working
	texture.repeat.set( 1, 1 ); 
	texture2.repeat.set( 1, 1 ); 


	floorMaterial = new THREE.MeshLambertMaterial({ map : texture });
	floorMaterial2 = new THREE.MeshLambertMaterial({ map : texture2 });

	floorMesh = new THREE.Mesh( new THREE.PlaneGeometry( 10000, 1000, 20, 20), floorMaterial);
	floorMesh2 = new THREE.Mesh( new THREE.PlaneGeometry( 10000, 2000, 20, 20), floorMaterial2 );


	floorMesh.side = THREE.DoubleSide;
	floorMesh2.side = THREE.DoubleSide;

	//floorMesh.receiveShadow = true;
	// rotate floor so it lays flat
	floorMesh.rotation.x = -90 * (Math.PI / 180);
	floorMesh2.applyMatrix( new THREE.Matrix4().makeTranslation( 0, 998, -450) );
	
	//floorMesh.receiveShadow = true;
	// rotate floor so it lays flat
	
	scene.add( floorMesh );
	scene.add( floorMesh2 );




}
function setupMarine()
{
	loader1.load('marine/marine.js', createMarine);
}

function createMarine(geometry, materials)
{
		var skinnedMesh = new THREE.SkinnedMesh(geometry, new THREE.MeshFaceMaterial(materials));
    	skinnedMesh.position.set(500, 0, 0);
    	skinnedMesh.rotation.y = Math.PI * 180 / 180;
    	skinnedMesh.scale.set(2, 2, 2);

    	scene.add(skinnedMesh);
     
    	animate(skinnedMesh);
}

function setupCube2() {
	var loader = new THREE.JSONLoader();
	loader.load( "cube.js", createScene1);

}

function createScene1(geometry, materials ) {
materials[ 0 ].shading = THREE.FlatShading;

	window.cube = new THREE.Mesh( geometry, new THREE.MeshFaceMaterial( materials ) );
	window.cube.position.x = 400;


	window.cube.applyMatrix( new THREE.Matrix4().makeTranslation( 0, 100, 0) );
		window.cube.scale.x = window.cube.scale.y = window.cube.scale.z = 50;
	scene.add( window.cube );
}

// animation loop
function render() 
{
	// animate scene
	renderer.render( scene, camera );
	// update camera controls
	//camera.lookAt( floorMesh.position );
	controls.update( clock.getDelta() );
	
	for (var i = bullets.length - 1; i >= 0; i--) {
		bullets[i].position.x += 50;

	}
	if (animation) animation.update( clock.getDelta() );
	// animate
	requestAnimationFrame( render);
	
}

var shoot = (function() {
	var negativeZ = new THREE.Vector3(1, 0, 0);

	return function(from, to) {
		from = from || window.enemy;
		bullet = new Bullet();
		bullet.position.copy(from.position);
		bullet.rotation.copy(from.rotation);
		if (to) {
			bullet.direction = to.position.clone().sub(from.position).normalize();
		}
		else {
			bullet.direction = negativeZ.clone().applyQuaternion(from.quaternion);
		}



		bullets.push(bullet);
		scene.add(bullet);
	};
})();


document.addEventListener('click', function(event) {
	
		event.preventDefault();
		shoot();

});

function animate(skinnedMesh) {
    var materials = skinnedMesh.material.materials;
 
    for (var k in materials) {
        materials[k].skinning = true;
    }
 
    THREE.AnimationHandler.add(skinnedMesh.geometry.animation);
    animation = new THREE.Animation(skinnedMesh, "ArmatureAction", THREE.AnimationHandler.CATMULLROM);
    animation.play();
}